//
//  ViewController.swift
//  KitchenGuide
//
//  Created by kapil vallamkonda on 6/4/20.
//  Copyright © 2020 kapil vallamkonda. All rights reserved.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController {
    @IBOutlet weak var signInButton: GIDSignInButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }


    @IBAction func getStartedClicked(_ button: UIButton) {
        guard let vc = UIStoryboard(name: "HomeViewController", bundle: nil).instantiateInitialViewController() as? HomeViewController else {
            return
        }
        guard let window = UIApplication.shared.keyWindow else { return }
        window.rootViewController = vc
        UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations: {}, completion:
        { completed in
            UserDefaults.standard.set(false, forKey: "isFirstTime")
        })
    }
}


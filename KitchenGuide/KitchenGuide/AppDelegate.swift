//
//  AppDelegate.swift
//  KitchenGuide
//
//  Created by kapil vallamkonda on 6/4/20.
//  Copyright © 2020 kapil vallamkonda. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window:UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        let isFirstTime = UserDefaults.standard.bool(forKey: "isFirstTime")
        if isFirstTime == false {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let homeVC = UIStoryboard(name: "HomeViewController", bundle: nil).instantiateInitialViewController() as! HomeViewController
            window?.rootViewController = homeVC
            window?.makeKeyAndVisible()
        }
        return true
    }
}

extension AppDelegate: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      // ...
      if let error = error {
        // ...
        return
      }

      guard let authentication = user.authentication else { return }
//      let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
//                                                        accessToken: authentication.accessToken)
        //GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
      // ...
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
